import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { GithubComponent } from './github/github.component';
import { AngularFireModule } from 'angularfire2';
import { FavoritesComponent } from './favorites/favorites.component';

export const firebaseConfig = {
  apiKey: "AIzaSyAeUlteVN5kYc47OCU2wG6eIPuqtuvXM0w",
  authDomain: "githubsearch-dd903.firebaseapp.com",
  databaseURL: "https://githubsearch-dd903.firebaseio.com",
  storageBucket: "githubsearch-dd903.appspot.com",
  messagingSenderId: "985794940754"
};

const routes:Routes = [
  {
    path: 'favorites',
    component: FavoritesComponent
  },
  {
    path: '',
    component: GithubComponent
  }

];

@NgModule({
  declarations: [
    AppComponent,
    GithubComponent,
    FavoritesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
