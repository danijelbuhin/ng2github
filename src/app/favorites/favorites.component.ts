import { Component, OnInit } from '@angular/core';
import {GithubService} from '../github.service'
import { AngularFire, FirebaseListObservable } from 'angularfire2';
@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.sass'],
  providers:[GithubService, AngularFire]
})
export class FavoritesComponent implements OnInit {
  user:any;
  username:any;
  repos:any;
  userFavList: FirebaseListObservable<any>;
  currentUser:any;
  users: FirebaseListObservable<any>;
  constructor(private af: AngularFire, private gs: GithubService){
  	this.af.auth.subscribe(auth => {
  		console.log(auth);
  		this.currentUser = auth;
  		if(auth){
  			this.af.database.list('/users').update(auth.uid, {
	  			name: auth.auth.displayName,
	  			photoURL: auth.auth.photoURL,
	  			uid: auth.uid
  			});

  			this.users = this.af.database.list(`/users/${this.currentUser.uid}/favorites`);
  		}
  		
  	});
  }

  deleteUser(user){
  	this.users.remove(user);
  }
  ngOnInit() {
  }

}
