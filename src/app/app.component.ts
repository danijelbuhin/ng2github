import { Component } from '@angular/core';
import { AngularFire, AuthProviders, AuthMethods, FirebaseListObservable } from 'angularfire2';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  providers: [AngularFire]
})
export class AppComponent {
  user: FirebaseListObservable<any>;
  currentUser:any;
  constructor(private af: AngularFire){
  	this.af.auth.subscribe(auth => {
  		console.log(auth);
  		this.currentUser = auth;
  		if(auth){
  			this.af.database.list('/users').update(auth.uid, {
	  			name: auth.auth.displayName,
	  			photoURL: auth.auth.photoURL,
	  			uid: auth.uid
  			});
  		}
  		
  	});
  }

  fbLogin(){
  	this.af.auth.login({
  		provider: AuthProviders.Facebook,
  		method: AuthMethods.Popup
  	})
  }
  googleLogin(){
  	this.af.auth.login({
  		provider: AuthProviders.Google,
  		method: AuthMethods.Popup
  	})
  }
  logOut(){
  	this.af.auth.logout();
  }

}
