import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class GithubService {
  private username:string;
  private client_id = '658068ad9b22037f57c0';
  private client_secret = '11cb410e6f0eac2d66c7904ae703194df576b8ec';

  constructor(private http: Http) {

  	console.log('service init')

  }

  getUser(){
  	return this.http.get(`https://api.github.com/users/${this.username}`).map(res => res.json());
  }
  getRepos(){
  	return this.http.get(`https://api.github.com/users/${this.username}/repos`).map(res => res.json());
  }
  updateUser(username:string){
  	this.username = username;
  }

}
