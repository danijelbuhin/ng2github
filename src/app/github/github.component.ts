import { Component, OnInit } from '@angular/core';
import {GithubService} from '../github.service'
import { AngularFire, FirebaseListObservable } from 'angularfire2';
@Component({
  selector: 'app-github',
  templateUrl: './github.component.html',
  styleUrls: ['./github.component.sass'],
  providers:[GithubService, AngularFire]
})
export class GithubComponent implements OnInit {
  user:any;
  username:any;
  repos:any;
  userFavList: FirebaseListObservable<any>;
  currentUser:any;
  users: FirebaseListObservable<any>;
  constructor(private gs:GithubService, private af: AngularFire) {

  	this.af.auth.subscribe(auth => {
  		console.log(auth);
  		this.currentUser = auth;
  		if(auth){
  			this.af.database.list('/users').update(auth.uid, {
	  			name: auth.auth.displayName,
	  			photoURL: auth.auth.photoURL,
	  			uid: auth.uid
  			});

  			this.users = this.af.database.list(`/users/${this.currentUser.uid}/favorites`);

  		}
  		
  	});
  }

  searchUsers(){
  	console.log(this.username);

  	//gs = Github Service
  	this.gs.updateUser(this.username);

  	this.gs.getUser().subscribe((res) => {
  		console.log(res);
  		this.user = res;
  	});
  	this.gs.getRepos().subscribe((res) => {
  		console.log(res);
  		this.repos = res;
  	});

  }

  addToFavorites(userName, userId, userLogin, userLink, thumb){
  	this.af.database.list(`/users/${this.currentUser.uid}/favorites`).update(userLogin, {
  		name: userName,
  		uid: userId,
  		login: userLogin,
  		url: userLink,
  		thumb: thumb,
  		bookmarked: true
  	});
  }
  ngOnInit() {
  }

}
