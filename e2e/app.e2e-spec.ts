import { Ng2githubPage } from './app.po';

describe('ng2github App', function() {
  let page: Ng2githubPage;

  beforeEach(() => {
    page = new Ng2githubPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
